# Sentennials Squad Discord Bot
Thanks for your interest in my bot. I originally wrote it just for our personal use on the [Collateral Damage discord server](https://discord.io/collateraldamage "Collateral Damage discord server"). 

## Attribution
If you use this bot, please add `Created by Sentennial#1234` in the bot's public bio.

## Features
### Automatic handling of server whitelists.
Bot will collect SteamIDs from users, and will upload those SteamIDs to the server assuming the user has the correct role from Patreon (requires user to link their Steam to Patreon, and for the Patreon owner to assign a role to a subscription tier). 
### Layer Rotation Uploading and Display
Bot will accept text files uploaded to a certain channel, and automatically upload them to the server. Bot will also post the current layer rotation in a public-facing channel automatically. 
### Ban Review Queue
Bot will listen for any posts made in a certain channel, upon a new post being created, bot will apply two reactions to the post and add that post to a list of bans needing review. Once the ban is reviewed and dealt with, you can click one of the reactions on the original post to remove it from the queue.

If somehow a ban gets stuck in the queue for any reason, an Executive can use `!banremove messageidofban` to remove it. Get the messageID of a ban in queue by right-clicking the `link to ban` hyperlink, the messageID is the end part of the link.

## Requires
[Install Docker](https://docs.docker.com/engine/install/ "Docker") and [Install Docker-Compose](https://docs.docker.com/compose/install/ "Docker-Compose")

## Initial Setup
- Create a folder for the bot, such as `squadbot`
- Copy all files from this repo to the folder
- In the folder, create folders named `squadconfig` and `sqlite`
- Set the perms to 777 with chmod on both folders
- Edit the src/config.cfg file and enable the features you want.
- Enter all the required configs for your Discord server. 
- Check the `docker-compose.yml` file. Ensure under `volumes` that the path pointing to the ServerConfig folder is correct. Default is `/home/squad/squadserver/data/SquadGame/ServerConfig`.Also check the second path, make sure it points to a folder inside the folder where the bot is installed, named `sqlite`. This is where the database file will live. 
- If using the Whitelist function, you need to add the following group permission to your Admins.cfg so the bot can use it: `Group=WhitelistBot:reserve`

## Run for production

`sudo docker-compose up -d --build`

## Check logs for errors
`sudo docker logs -f squadbot -n 50` assuming you left the container_name untouched in `docker-compose.yml`
